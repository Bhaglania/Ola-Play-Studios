package com.anupam.ola_play_studios.myAdaptor;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anupam.ola_play_studios.MyMusicPlayerService;
import com.anupam.ola_play_studios.R;
import com.anupam.ola_play_studios.modals.SongInfo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


public class SongsRecyclerAdaptor extends RecyclerView.Adapter<SongsRecyclerAdaptor.MyViewHolder> {
    SongInfo songInfos[];
    Context context;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public SongsRecyclerAdaptor(Context context, SongInfo songInfos[]) {
        this.songInfos = songInfos;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_song_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SongInfo songInfo = songInfos[position];
        holder.songName.setText(songInfo.getSong());
        holder.artistName.setText(songInfo.getArtists());
        String url = songInfo.getCover_image();
        Glide.with(context).
                load(url).
                placeholder(ContextCompat.getDrawable(context, R.drawable.ic_bubble_chart)).
                centerCrop().
                crossFade().
                diskCacheStrategy(DiskCacheStrategy.SOURCE).
                into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Playing Song " + songInfo.getSong(), Toast.LENGTH_LONG).show();
                Intent svc = new Intent(context, MyMusicPlayerService.class);
                svc.putExtra("songInfo", songInfo);
                context.startService(svc);
            }
        });
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imageView.callOnClick();
            }
        });
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    downloadSong(songInfo.getUrl(), songInfo.getSong());
                } catch (Exception e) {
                    Toast.makeText(context, "Restart App and grant us write permission to download file", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    private void downloadSong(String url, String song) {
        String message = "Downloading " + song + " to " + Environment.DIRECTORY_DOWNLOADS + "/Songs";
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(url);

        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle(song + ".mp3");
        request.setDescription("Downloading ");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/Songs/" + song + ".mp3");


        downloadManager.enqueue(request);
    }

    @Override
    public int getItemCount() {
        return songInfos.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView songName, artistName;
        ImageView imageView, pausePlay, download;
        LinearLayout parent;

        MyViewHolder(View itemView) {
            super(itemView);
            parent = (LinearLayout) itemView.findViewById(R.id.parent);
            artistName = (TextView) itemView.findViewById(R.id.artist);
            songName = (TextView) itemView.findViewById(R.id.songName);
            imageView = (ImageView) itemView.findViewById(R.id.coverImage);
            download = (ImageView) itemView.findViewById(R.id.download);
        }
    }
}
