package com.anupam.ola_play_studios.myUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anupam.ola_play_studios.modals.SongInfo;

import java.util.ArrayList;


public class MyDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "olaplaystudios";
    private static final String TABLE_SONGS_HISTORY = "songsHistory";
    private static final String COLUMN_NAME_SONG = "song";
    private static final String COLUMN_NAME_COVER_IMAGE = "cover_image";
    private static final String COLUMN_NAME_URL = "url";
    private static final String COLUMN_NAME_ID = "id";
    private static final String COLUMN_NAME_ARTIST = "artists";


    private static final String SQL_CREATE_TABLE_HISTORY =
            "CREATE TABLE IF NOT EXISTS " + TABLE_SONGS_HISTORY +
                    " ( " +
                    COLUMN_NAME_ID + " TEXT NOT NULL, " +
                    COLUMN_NAME_SONG + " TEXT NOT NULL, " +
                    COLUMN_NAME_COVER_IMAGE + " TEXT NOT NULL, " +
                    COLUMN_NAME_URL + " TEXT NOT NULL, " +
                    COLUMN_NAME_ARTIST + " TEXT NOT NULL, " +
                    " PRIMARY KEY ( " + COLUMN_NAME_ID + " ) " +
                    " )";


    private final String SQL_DROP_TABLE_HISTORY =
            "DROP TABLE IF EXISTS " + TABLE_SONGS_HISTORY;

    public MyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_HISTORY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // resetDatabase();

        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_HISTORY);

        //sqLiteDatabase.close();
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


    public void resetDatabase() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_HISTORY);
        onCreate(sqLiteDatabase);
        sqLiteDatabase.close();
    }

    public void deleteSongHistory() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_HISTORY);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_HISTORY);
        sqLiteDatabase.close();
    }

    public boolean insertSongInfo(SongInfo songInfo) {

        SQLiteDatabase sqLiteDatabase = null;
        boolean isSuccessful = false;
        try {
            sqLiteDatabase = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_ARTIST, songInfo.getArtists());
            values.put(COLUMN_NAME_SONG, songInfo.getSong());
            values.put(COLUMN_NAME_URL, songInfo.getUrl());
            values.put(COLUMN_NAME_ID, System.nanoTime());
            values.put(COLUMN_NAME_COVER_IMAGE, songInfo.getCover_image());

            long newRowId;
            newRowId = sqLiteDatabase.insert(
                    TABLE_SONGS_HISTORY,
                    null,
                    values);

            isSuccessful = newRowId != -1;
        } catch (Exception e) {
            isSuccessful = false;
            e.printStackTrace();
        } finally {
            try {
                if (sqLiteDatabase != null)
                    sqLiteDatabase.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return isSuccessful;
    }


    public SongInfo[] getSongsHistory() {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                COLUMN_NAME_ARTIST,
                COLUMN_NAME_SONG,
                COLUMN_NAME_URL,
                COLUMN_NAME_COVER_IMAGE,
        };

        String sortOrder =
                COLUMN_NAME_ID + " DESC";

        Cursor cursor = db.query(
                TABLE_SONGS_HISTORY,                  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        ArrayList<SongInfo> songInfos = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                SongInfo songInfo = new SongInfo();
                songInfo.setArtists(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_ARTIST)));
                songInfo.setSong(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_SONG)));
                songInfo.setUrl(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_URL)));
                songInfo.setCover_image(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_COVER_IMAGE)));
                songInfos.add(songInfo);

            } while (cursor.moveToNext());
        }

        try {
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return songInfos.toArray(new SongInfo[songInfos.size()]);
    }


}