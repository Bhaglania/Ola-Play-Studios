package com.anupam.ola_play_studios.myUtils;


import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class DownloadDataTask extends AsyncTask {

    static final String TAG = DownloadDataTask.class.getName();

    private OnDataDownloadListener listener;

    private String url;

    public DownloadDataTask(OnDataDownloadListener listener, String url) {
        this.listener = listener;
        this.url = url;
    }

    @Override
    protected void onPostExecute(Object o) {
        String data = (String) o;
        Log.e("dsf", data);
        listener.onDataDownloadSuccess(data);

    }

    @Override
    protected Object doInBackground(Object[] params) {
        return downloadData(url);
    }

    private String downloadData(String url) {

        HttpURLConnection con = null;
        try {
            URL u = new URL(url);
            con = (HttpURLConnection) u.openConnection();
            con.connect();

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            return sb.toString();


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return "";


    }

    public interface OnDataDownloadListener {
        void onDataDownloadSuccess(JSONObject result);

        void onDataDownloadSuccess(String result);

        void onDataDownloadError(String errorDescription);
    }


}