package com.anupam.ola_play_studios;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anupam.ola_play_studios.modals.SongInfo;
import com.anupam.ola_play_studios.myAdaptor.SongsRecyclerAdaptor;
import com.anupam.ola_play_studios.myUtils.DownloadDataTask;
import com.anupam.ola_play_studios.myUtils.MyDbHelper;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements DownloadDataTask.OnDataDownloadListener, MyMusicPlayerService.MediaPlayerProgressListener {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    FloatingActionButton floatingActionButton;
    SwipeRefreshLayout swipeRefreshLayout;
    SongInfo songsList[] = null;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    downloadMusicLibrary();
                    hideDeleteHistoryButton();
                    return true;
                case R.id.navigation_history:
                    MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
                    SongInfo[] songInfos = myDbHelper.getSongsHistory();
                    if (songInfos.length == 0) {
                        Toast.makeText(getApplicationContext(), "No History Found", Toast.LENGTH_LONG).show();
                    } else
                        addDeleteHistoryButton();
                    addSongsToView(songInfos);

                    return true;

            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyMusicPlayerService.listener = this;
        addSwipeToRefresh();
        downloadMusicLibrary();
        initializeBottomNavigationView();
        addMediaPlayerToActivity();
        askForFilePermissions();
    }

    private void askForFilePermissions() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1004);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1004:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Now You can download files", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "you will not be able to download file", Toast.LENGTH_LONG).show();
                }
                break;
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        MyMusicPlayerService.listener = this;
        addMediaPlayerToActivity();

    }

    private void addMediaPlayerToActivity() {
        View myPlayer = (View) findViewById(R.id.myPlayer);
        myPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PlayerActivity.class));
            }
        });
        final SongInfo songInfo = MyMusicPlayerService.currentSong;
        if (songInfo == null) {
            myPlayer.setVisibility(View.GONE);
            return;
        }
        myPlayer.setVisibility(View.VISIBLE);
        ImageView mpCoverImage = (ImageView) findViewById(R.id.mpCoverImage);
        Glide.with(MainActivity.this).
                load(songInfo.getCover_image()).
                centerCrop().
                placeholder(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bubble_chart)).
                into(mpCoverImage);
        TextView mpSongName, mpArtist;
        mpArtist = (TextView) findViewById(R.id.mpArtist);
        mpSongName = (TextView) findViewById(R.id.mpSongName);
        mpArtist.setText(songInfo.getArtists());
        mpSongName.setText(songInfo.getSong());
        ImageView mpPlay = (ImageView) findViewById(R.id.mpPlay);

        mpPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isPlayIng;
                try {
                    isPlayIng = MyMusicPlayerService.isPlaying();
                    pausePlayCurrentSong(isPlayIng, v);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (songInfo == null) {
                    onSongFinished();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Playing Song " + songInfo.getSong(), Toast.LENGTH_LONG).show();
                Intent svc = new Intent(getApplicationContext(), MyMusicPlayerService.class);
                svc.putExtra("songInfo", songInfo);
                startService(svc);

            }
        });


    }

    private void pausePlayCurrentSong(boolean isPlayIng, View v) {
        if (MyMusicPlayerService.currentSong == null) {
            onSongFinished();
            return;
        }
        ImageView imageView = (ImageView) v;
        Intent myIntent = new Intent(getApplicationContext(), MyMusicPlayerService.class);
        myIntent.putExtra("songInfo", MyMusicPlayerService.currentSong);
        myIntent.putExtra("stop", false);
        myIntent.putExtra("play", false);
        myIntent.putExtra("pause", true);
        Drawable drawable;
        if (isPlayIng) {
            drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_play_circle_filled_white);
            myIntent.putExtra("play", false);
            myIntent.putExtra("pause", true);

        } else {
            drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pause_circle_filled_white);
            myIntent.putExtra("play", true);
            myIntent.putExtra("pause", false);
        }
        ((ImageView) v).setImageDrawable(drawable);
        startService(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.search:
                onSearchRequested();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchForSong(query);
        }
    }

    private void searchForSong(String query) {
        try {
            if (songsList == null && query == null) {
                return;
            }
            ArrayList<SongInfo> songInfos = new ArrayList<>();
            query = query.toLowerCase();
            for (int i = 0; i < songsList.length; i++) {
                SongInfo songInfo = songsList[i];
                String song = songInfo.getSong().toLowerCase();
                String artists = songInfo.getArtists().toLowerCase();
                if (song.contains(query)) {
                    songInfos.add(songInfo);
                } else if (artists.contains(query)) {
                    songInfos.add(songInfo);
                }
            }
            if (songInfos.size() == 0) {
                Toast.makeText(getApplicationContext(), "Coudnt find result", Toast.LENGTH_LONG).show();
                return;
            }
            addSongsToView(songInfos.toArray(new SongInfo[songInfos.size()]));
            addCloseOption();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCloseOption() {
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setVisibility(View.VISIBLE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSongsToView(songsList);
                floatingActionButton.setVisibility(View.GONE);
            }
        });
    }

    private void initializeBottomNavigationView() {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void hideDeleteHistoryButton() {
        FloatingActionButton floatingActionButtonDelete = (FloatingActionButton) findViewById(R.id.floatingActionButtonDelete);
        floatingActionButtonDelete.setVisibility(View.GONE);
    }

    private void addDeleteHistoryButton() {
        FloatingActionButton floatingActionButtonDelete = (FloatingActionButton) findViewById(R.id.floatingActionButtonDelete);
        floatingActionButtonDelete.setVisibility(View.VISIBLE);
        floatingActionButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
                myDbHelper.deleteSongHistory();
                myDbHelper.close();
                hideDeleteHistoryButton();
                addSongsToView(new SongInfo[0]);
            }
        });

    }

    private void addSwipeToRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadMusicLibrary();
            }
        });
    }

    private void downloadMusicLibrary() {
        showRefreshing();
        String url = "http://starlord.hackerearth.com/studio";
        new DownloadDataTask(this, url).execute();
    }

    private void showRefreshing() {
        if (swipeRefreshLayout != null) {
            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(true);
            }
        }
    }

    @Override
    public void onDataDownloadSuccess(JSONObject result) {


    }

    @Override
    public void onDataDownloadSuccess(String result) {
        hideRefreshing();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        SongInfo songInfos[] = gson.fromJson(result, SongInfo[].class);
        songsList = songInfos;
        addSongsToView(songInfos);
    }

    private void hideRefreshing() {
        if (swipeRefreshLayout != null) {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void addSongsToView(SongInfo[] songInfos) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.songsList);
        recyclerView.removeAllViews();
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        SongsRecyclerAdaptor repositoriesAdaptor = new SongsRecyclerAdaptor(getApplicationContext(), songInfos);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(repositoriesAdaptor);
    }

    @Override
    public void onDataDownloadError(String errorDescription) {

    }

    @Override
    public void onSongFinished() {
        View myPlayer = (View) findViewById(R.id.myPlayer);
        myPlayer.setVisibility(View.GONE);
    }

    @Override
    public void onProgressChanged(int progress, int max) {

    }

    @Override
    public void onNewSongAdded(SongInfo songInfo) {
        addMediaPlayerToActivity();
    }
}
