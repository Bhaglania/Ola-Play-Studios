package com.anupam.ola_play_studios;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.anupam.ola_play_studios.modals.SongInfo;
import com.bumptech.glide.Glide;

public class PlayerActivity extends AppCompatActivity
        implements MyMusicPlayerService.MediaPlayerProgressListener {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        SongInfo songInfo = MyMusicPlayerService.currentSong;
        addSongDescription(songInfo);
        addPlayerControls();
    }

    private void addSongDescription(final SongInfo songInfo) {
        setImage(songInfo.getCover_image());
        TextView textView = (TextView) findViewById(R.id.songName);
        TextView songNameBold = (TextView) findViewById(R.id.songNameBold);
        textView.setText(songInfo.getSong());
        songNameBold.setText(songInfo.getSong());
        TextView artists = (TextView) findViewById(R.id.artists);
        artists.setText(songInfo.getArtists());
    }

    private void addPlayerControls() {
        ImageView imageViewPlay, forward, backWard;
        imageViewPlay = (ImageView) findViewById(R.id.imageViewPlay);
        forward = (ImageView) findViewById(R.id.forward);
        backWard = (ImageView) findViewById(R.id.backWard);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        MyMusicPlayerService.listener = this;
        imageViewPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isPlayIng = MyMusicPlayerService.isPlaying();
                Intent myIntent = new Intent(getApplicationContext(), MyMusicPlayerService.class);
                myIntent.putExtra("songInfo", MyMusicPlayerService.currentSong);
                myIntent.putExtra("stop", false);
                myIntent.putExtra("play", false);
                myIntent.putExtra("pause", true);
                if (isPlayIng) {
                    myIntent.putExtra("play", false);
                    myIntent.putExtra("pause", true);

                } else {
                    myIntent.putExtra("play", true);
                    myIntent.putExtra("pause", false);
                }
                startService(myIntent);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MyMusicPlayerService.forwardSong();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        backWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MyMusicPlayerService.reverseSong();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setImage(String url) {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Glide.with(PlayerActivity.this).
                load(url).
                placeholder(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bubble_chart)).
                into(imageView);
    }

    @Override
    protected void onDestroy() {
        MyMusicPlayerService.listener = null;
        super.onDestroy();
    }

    @Override
    public void onSongFinished() {

    }

    @Override
    public void onProgressChanged(int progress, int max) {
        seekBar.setMax(max);
        seekBar.setProgress(progress);
    }

    @Override
    public void onNewSongAdded(SongInfo songInfo) {
        addSongDescription(songInfo);
        addPlayerControls();
    }
}
