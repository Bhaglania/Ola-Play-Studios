package com.anupam.ola_play_studios;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.anupam.ola_play_studios.modals.SongInfo;
import com.anupam.ola_play_studios.myUtils.MyDbHelper;


public class MyMusicPlayerService extends Service {

    private static final int NOTIF_ID = 12345;
    private static final String TAG = null;
    public static SongInfo currentSong = null;
    static MediaPlayerProgressListener listener = null;
    private static MediaPlayer mediaPlayer;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private boolean playPause;
    private boolean initialStage = true;

    public static boolean isPlaying() {
        if (mediaPlayer == null) {
            return false;
        }
        boolean isPlaying;
        try {
            isPlaying = mediaPlayer.isPlaying();
        } catch (Exception e) {
            isPlaying = false;
        }
        return isPlaying;
    }

    public static void forwardSong() throws Exception {
        if (mediaPlayer.isPlaying()) {
            int max = mediaPlayer.getDuration();
            int pos = mediaPlayer.getCurrentPosition() + 10000;
            if (pos >= max) {
                mediaPlayer.seekTo(max);
            } else
                mediaPlayer.seekTo(pos);
        }
    }

    public static void reverseSong() throws Exception {

        if (mediaPlayer.isPlaying()) {
            int pos = mediaPlayer.getCurrentPosition() - 10000;
            if (pos > 0)
                mediaPlayer.seekTo(pos);
            else
                mediaPlayer.seekTo(pos);
        }
    }

    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        boolean isAction = checkForAction(intent);
        if (isAction) {
            return START_STICKY;
        }
        SongInfo songInfo = (SongInfo) intent.getExtras().get("songInfo");
        playSong(songInfo);
        return START_STICKY;
    }

    private void playSong(SongInfo songInfo) {
        if (mediaPlayer != null)
            try {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


        startForeGroundService(getApplicationContext(), songInfo, true);
        currentSong = songInfo;
        mediaPlayer.start();
        new Player().execute(songInfo.getUrl());

        addSongToHistory(songInfo, getApplicationContext());
    }

    void addSongToHistory(SongInfo songInfo, Context context) {
        MyDbHelper myDbHelper = new MyDbHelper(context);
        myDbHelper.insertSongInfo(songInfo);
        myDbHelper.close();
    }

    private boolean checkForAction(Intent intent) {

        boolean pause = intent.getBooleanExtra("pause", false);
        if (pause) {
            mediaPlayer.pause();
            SongInfo songInfo = (SongInfo) intent.getExtras().get("songInfo");
            startForeGroundService(getApplicationContext(), songInfo, false);
            return true;
        }
        boolean play = intent.getBooleanExtra("play", false);
        if (play) {
            SongInfo songInfo = (SongInfo) intent.getExtras().get("songInfo");
            playSong(songInfo);
            return true;
        }
        boolean stop = intent.getBooleanExtra("stop", false);
        if (stop) {
            stopBackgroundService();
            return true;
        }
        return false;
    }

    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {

    }

    public void onPause() {

    }

    @Override
    public void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    @Override
    public void onLowMemory() {

    }

    private void sendSongAddedAlert() {
        if (listener != null)
            listener.onNewSongAdded(currentSong);
    }

    private void updateUIForProgress() {

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean run = true;
                        if (mediaPlayer == null) {
                            return;
                        }
                        if (!mediaPlayer.isPlaying()) {
                            return;
                        }
                        int pos = mediaPlayer.getCurrentPosition();
                        int max = mediaPlayer.getDuration();
                        if (pos >= max) {

                        } else {
                            updateUIForProgress();
                        }
                        if (listener != null)
                            listener.onProgressChanged(pos, max);
                    } catch (Exception E) {
                        E.printStackTrace();
                    }
                }
            }, 200);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void startForeGroundService(Context context, SongInfo songInfo, boolean isPaused) {
        String msg = songInfo.getArtists();
        String heading = songInfo.getSong();

        Intent intent1 = new Intent(context, PlayerActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent1, 0);
        boolean start = true;


        if (start) {

            PendingIntent pendingIntentStop = getStopPendingIntent(songInfo, context);
            PendingIntent pendingIntentPause = getPausePauseIntent(songInfo, context);
            int drawable = R.drawable.ic_pause_circle_filled;
            String actionName = "Pause";
            if (!isPaused) {
                actionName = "play";
                drawable = R.drawable.ic_play_circle_filled;
                pendingIntentPause = getPlayPendingIntent(songInfo, context);
            }

            Notification myNotification;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                myNotification = new NotificationCompat.Builder(context)
                        .setContentTitle(heading)
                        .setContentText(msg)
                        .setTicker("Ola-play-Studios")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .addAction(R.drawable.ic_stop_black, "Stop", pendingIntentStop)
                        .addAction(drawable, actionName, pendingIntentPause)
                        .setSmallIcon(R.drawable.ic_bubble_chart)
                        .build();
            } else {
                myNotification = new NotificationCompat.Builder(context)
                        .setContentTitle(heading)
                        .setContentText(msg)
                        .setTicker("Ola-play-Studios")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.ic_bubble_chart)
                        .build();
            }
            startForeground(MyMusicPlayerService.NOTIF_ID, myNotification);
        } else {
            stopBackgroundService();
        }
    }

    private PendingIntent getStopPendingIntent(SongInfo songInfo, Context context) {
        Intent myIntent = new Intent(getApplicationContext(), MyMusicPlayerService.class);
        myIntent.putExtra("songInfo", songInfo);
        myIntent.putExtra("stop", true);
        myIntent.putExtra("play", false);
        myIntent.putExtra("pause", false);
        return PendingIntent.getService(context, 12, myIntent, 0);
    }

    private PendingIntent getPlayPendingIntent(SongInfo songInfo, Context context) {
        Intent myIntent = new Intent(getApplicationContext(), MyMusicPlayerService.class);
        myIntent.putExtra("songInfo", songInfo);
        myIntent.putExtra("stop", false);
        myIntent.putExtra("play", true);
        myIntent.putExtra("pause", false);
        return PendingIntent.getService(context, 11, myIntent, 0);
    }

    private PendingIntent getPausePauseIntent(SongInfo songInfo, Context context) {
        Intent myIntent = new Intent(getApplicationContext(), MyMusicPlayerService.class);
        myIntent.putExtra("songInfo", songInfo);
        myIntent.putExtra("stop", false);
        myIntent.putExtra("play", false);
        myIntent.putExtra("pause", true);
        return PendingIntent.getService(context, 10, myIntent, 0);
    }

    private void stopBackgroundService() {
        try {
            if (listener != null)
                listener.onSongFinished();
            stopForeground(true);
            stopSelf();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface MediaPlayerProgressListener {
        void onSongFinished();

        void onProgressChanged(int progress, int max);

        void onNewSongAdded(SongInfo songInfo);
    }

    class Player extends AsyncTask<String, Void, Boolean> {
        Boolean prepared = false;

        @Override
        protected Boolean doInBackground(String... strings) {
            prepared = false;

            try {
                mediaPlayer.setDataSource(strings[0]);
                Handler handler = new Handler(Looper.getMainLooper()) {
                    @Override
                    public void handleMessage(Message message) {

                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                initialStage = true;
                                playPause = false;
                                mediaPlayer.stop();
                                mediaPlayer.reset();
                                if (listener != null)
                                    listener.onSongFinished();
                                stopBackgroundService();
                            }
                        });
                        try {
                            mediaPlayer.prepare();
                            prepared = true;
                            sendSongAddedAlert();
                            updateUIForProgress();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                };
                Message message = handler.obtainMessage(9, "");
                message.sendToTarget();


            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MyAudioStreamingApp", "" + e);
                prepared = false;
            }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mediaPlayer.start();
            initialStage = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

}
